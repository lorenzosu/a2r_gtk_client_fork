"""
A patch parser aimed at counstructing a GUI for communicating with a Pure Data
patch (to and fro). Based on the semantics specified here:

http://intern.radiocorax.de/projects/a2r/wiki/Pd_-_GUI_communication_semantics_ideas

The parser basically has to search for [s] ([send]) and [r] ([receive]) objects
formatted in the way specified by the semantics (in particular using a $0- to begin)

Then construct a Gtk gui appropriately. 

There has to be some mechanism to set-up gui elements which need to send data TO
Pd and vice-versa. For sending this will need to be done via callbacks.
For receiving, this will have to be implemented in some other way, e.g. creating
a reciver dictionary which associates the message with a method related to
modifying the gui element

In both cases we also need a dictionary of functions to modify or access the
widget data appropriately.

For example in pygtk a scale (a slider) has set_value and get_value methods, a
label has set_text, get_text etc.

Maybe we can think of generic callbacks like label_setter, label_getter, etc.
which we connect to 

Grouping. For now we use VBox (anf Hscale) as default...

GTK widgets for *sending* to Pd:

hslider: horizontal slider -> gtk.HScale
vslider: vertical slider -> gtk.VScale
knob: a knob control -> ??
bang: bang button -> gtk.button (text specified in the r)
toggle: toggle -> gtk.button (with "I" "0" e.g...)
text: text input -> gtk.Entry possibly with a button, e.g. if for a filename
number: numerical input -> gtk.Entry
range (?): a control representing a number range (begin and end value) -> ?

GTK widgets for receiving from Pd:
number: a number -> gtk.Label (or entry)
text: text output
(text and number could both be handled as text (e.g. 'label') for output purposes.
VU: a vumeter control -> gtk.ProgressBar (for now
array: representation of an array -> ???


TODO: How do we return the gui finally?
"""
import gtk
import gobject
import socket
import pdserver

WIDGET_DICTIONARY = {
        'hslider' : gtk.HScale,
        'vslider' : gtk.VScale,
        'knob' : None,
        'bang' : gtk.Button,
        'toggle': gtk.Button,
        'text': gtk.Entry,
        'number': gtk.Entry,
        'range': None,
        'vu': gtk.ProgressBar
        }
METHOD_DICTRIONARY = {
        gtk.Entry: (gtk.Entry.set_text, str),
        gtk.ProgressBar: (gtk.ProgressBar.set_fraction, float)
        }

class Widget:
    """ A class representing a Pd widget """
    def __init__(self, wtype, wname, is_output):
        self.type = wtype
        self.name = wname
        self.is_output = is_output
        self.gtk_widget = None #this doesn't exist at first

    def __eq__(self, other):
        return (
                (self.type == other.type) and
                (self.name == other.name) and
                (self.is_output == other.is_output)
                )

class PatchParser:
    """ A class to parse the Pd patch """
    def __init__(self, filename=None):
        self.filename = filename
        self.groups = {}

    def find_widgets(self):
        """ parse the patch file and find widgets """
        try:
            f = open(self.filename,'r')
        except:
            return -1
        for line in f.readlines():
            result = self.find_receives(line)
            result2 = self.find_sends(line)
            if result != None:
              g, w, n = result
              w = Widget(w, n, True)
            elif result2 != None:
              g, w, n = result2
              w = Widget(w, n, False)
            else:
                continue

            if g not in self.groups:
                self.groups[g] = []
            if w not in self.groups[g]: # do not add already existing widgets
                self.groups[g].append(w)


        f.close()
    def find_receives(self, text_line):
        """ find a receive widgets in a line from the Pd patch Pd.
        i.e. output widgets in the gui (GUI -> Pd)
        If succeswfull return a tuple: group, wideget, name"""
        needle = 'r \$0-'
        index = text_line.find (needle)
        if index < 0:
            return None
        search_string = text_line[text_line.find(needle) + len(needle):-2]
        g, w, n = search_string.split('.')
        return (g, w, n)

    def find_sends(self, text_line):
        needle = 'togui-'
        index = text_line.find (needle)
        if index < 0:
            return None
        search_string = text_line[text_line.find(needle) + len(needle):-2]
        search_string = search_string.split()[0]
        g, w, n = search_string.split('.')
        return (g, w, n)

    def print_widgets(self):
        inout = {True : 'OUTPUT', False: 'INPUT'}
        for k in self.groups.keys():
            print "GROUP:",k
            for w in self.groups[k]:
                print "--",w.type, w.name, inout[w.is_output], type(w.gtk_widget)

    def generate_gui(self, pd_server):
        keys = self.groups.keys()
        if len(keys) == 0:
            return None
        gui_container = gtk.VBox()
        t = gtk.Tooltips()
        in_widgets = {}
        for k in keys:
            g = gtk.VBox()
            g.set_border_width(4)
            for w in self.groups[k]:
                widg_type = WIDGET_DICTIONARY[w.type]
                widg = widg_type()
                # tuple containing the widget group, widget, and server to pass
                # to the callback
                callback_data = (k, w, pd_server)
                widget_key = ('%s.%s.%s') % (k,w.type,w.name)               
                # widget-specific settings
                if type(widg) in [gtk.Button]:
                    widg.set_label(w.name.replace('_',' '))
                    if w.is_output:
                        widg.connect('clicked', self.sender_callback, callback_data)
                elif type(widg) in [gtk.HScale, gtk.VScale]:
                    print "it's a scale"
                    adj = gtk.Adjustment(0.0, 0.0, 1.0, 0.01, 0.1, 0.0)
                    widg = widg_type(adj)
                    widg.set_digits(2)
                    if w.is_output:
                        widg.connect("value-changed", self.sender_callback, (callback_data))
                elif type(widg) in [gtk.ProgressBar]:
                    if not w.is_output:
                        in_widgets[widget_key] = widg
                elif type(widg) in [gtk.Entry]:
                    if not w.is_output:
                        widg.set_editable(False)
                        in_widgets[widget_key] = widg


                t.set_tip(widg, w.name.replace('_',' '))
                widg.show()
                g.pack_start(widg)
                g.show_all()
                #TODO handle input/output callbacks
                w.gtk_widget = widg

            f = gtk.Frame(k)
            f.set_border_width(4)
            f.add(g)
            gui_container.pack_start(f, True, True)
        return (gui_container, in_widgets)

    def sender_callback(self, widget, callback_data):
        """ This is the generic sender callback, which sends data from the
        widgets to pd
        """
        group = callback_data[0]
        our_widget = callback_data[1]
        pd_server = callback_data[2]
        format_string = ("%s.%s.%s") % (group, our_widget.type, our_widget.name)
        if type(widget) in [gtk.Button]:
            print ("%s clicked") % (format_string)
            pd_server.out_queue(("destination %s, message bang;") % (format_string))
            # sockclient.sendall(("destination %s, message bang;") % (format_string))
        elif type(widget) in [gtk.HScale, gtk.VScale]:
            print ("%s value is: %f") % (format_string, widget.get_value())
            pd_server.out_queue(("destination %s, message %f;") % (format_string, widget.get_value()))
        #sockclient.sendall(("destination %s, message %f;") % (format_string, widget.get_value()))
        pd_server.send_message()

def receiver(pd_server, widget_dict, update_list):
    #print "- check for in_message"
    #print widget_dict
    if pd_server.inmessage != '':
        the_message = pd_server.inmessage.strip()
        for m in the_message.split(';'):
            m = m.replace('togui-','')
            try:
                k, val = m.split(' ')
                w = widget_dict[k]
                update_list.append((w, val))
            except:
                continue
        pd_server.flush_inmessage()

    return True

def in_updater(u_list):
    #print u_list
    for tup in u_list:
        w = tup[0]
        v = tup[1]
        method = METHOD_DICTRIONARY[type(w)][0]
        message_type = METHOD_DICTRIONARY[type(w)][1]
        #print method
        method(w, message_type(v))
    u_list = []
    return True

def _test():
    print "### Start test ###"
    pd_server = pdserver.PdServer()
    pd_server.connect_client()
    #pp = PatchParser('kiosk1.pd')
    pp = PatchParser('kiosk2.pd')
    pp.find_widgets()
    gui_generated = pp.generate_gui(pd_server)
    gui = gui_generated[0]
    in_widget_dict = gui_generated[1]
    pp.print_widgets()
    gui.show()
    up_list = []
    gobject.idle_add(receiver, pd_server, in_widget_dict, up_list)
    gobject.idle_add(in_updater, up_list)
    w = gtk.Window()
    w.add(gui)
    w.connect("delete_event", lambda q,w: gtk.mainquit())
    w.show_all()
    gtk.main()

# Uncomment to test
_test()

