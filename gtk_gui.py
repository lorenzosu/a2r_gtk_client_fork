""" Demo of GTK gui with client and server to send/receive to/from Pd 

    Credits: socket documentation, gobject documentation (and pygtk FAQ)
    This was also very useful: http://rox.sourceforge.net/desktop/node/413.html
    for the listener and handler funcions (taken as is)
"""
import gobject
import gtk
import socket
import random
 
class App:
    def __init__(self):
	""" Server and start listening. Client, Then gui """
        # SERVER
        self.serverhost = ''
        self.serverport = 4322
	self.sockserver = socket.socket()
	self.sockserver.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	self.sockserver.bind((self.serverhost, self.serverport))
	self.sockserver.listen(1)
	print "Listening..."
	gobject.io_add_watch(self.sockserver, gobject.IO_IN, self.listener)
        print "Added io_add_watch for socket"
        # CLIENT
        self.clienthost = ''
        self.clientport = 4322
        self.sockclient = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect_client()
        # GUI part
        self.win = gtk.Window()
        self.win.connect("delete_event",lambda q,w: gtk.mainquit())
        self.box = gtk.HBox()
        self.button = gtk.Button("Button 1")
        self.button.connect("clicked", self.button1_click_callback)
        self.button.show()
        self.button2 = gtk.Button("something else")
        self.button2.connect("clicked",self.button2_click_callback)
        self.button2.show()
        self.pbar = gtk.ProgressBar()
        self.pbar.show()
        self.box.pack_start(self.button)
        self.box.pack_start(self.button2)
        self.box.pack_start(self.pbar)
        self.box.show()
        self.win.add(self.box)
        self.win.show()

    def connect_client(self):
        """ Connect as client """
        try:
            self.sockclient.connect((self.clienthost, self.clientport))
            self.client_connected = True
            print "Connected to server on port %d" % self.clientport
        except:
            self.client_connected = False
            print "no server (yet?)..."

    def close_client(self):
        """ Close the client connection """
        pass

    def button1_click_callback(self, widget):
        """ Send a random value for 'volume' in FUDI """
        print "Button 1"
        if not self.client_connected:
            self.connect_client()
        if not self.client_connected:
            print "Cannot connect..."
            return False
        self.sockclient.sendall(('volume %f;') % (random.random()))
        
    def button2_click_callback(self, widget):
        """ Just do something when the other button is clicked """
        print "Clicked button..."
 
    def listener(self, sock, condition):
        """ Asynchronous connection listener.
        Starts a handler for each connection."""
        self.conn, self.addr = self.sockserver.accept()
        print "Connection to Server:", self.addr
        gobject.io_add_watch(self.conn, gobject.IO_IN, self.handler)
        return True
 
 
    def handler(self, conn, condition):
        """ Asynchronous connection handler.
        Processes each line from the socket. """
        data = self.conn.recv(4096)
        if not len(data):
                print "Connection closed - " + str(self.addr)
                return False
        else:
                data = data.strip()
                if data.find('bar') == 0:
                    try:
                        value = data[0:data.find(';')]
                        value = value.split(' ')[1]
                        self.pbar.set_fraction(float(value))
                    except:
                        print "Bad value in %s" % data
                print ("Received: %s") % (data)
                return True
 
if __name__=='__main__':
    app = App()
    gtk.main()
