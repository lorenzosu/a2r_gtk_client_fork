Pd - GUI communication semantics ideas
======================================

Idea: Create Pd patches which use simple semantics to expose controls to
an external generic GUI client/application which will in turn receive
feedback from Pd.

This is achieved by cabling the patch(es) with [receive] (a.k.a. [r])
and [send] (a.k.a. [s]) objects with names which follow these
semantics.
In this way the [r] are the controls that the Pd patch exposes to the
GUI, and the [s] are the values which [Pd] wishes to send back to the
GUI, for example for elements in the GUI which should react to the
patch’s evolution (e.g. a VUmeter responding to a signal, the end of a
file playback, a timer etc.)

The GUI application should be able to analyse the patch and a GUI be
built (semi)automatically for quick communication with the patch. Of
course further adjustments for the GUI can be done manually, but the
system should ideally be able to build up a sensible GUI for the patch
automatically: for this reason some kind of ‘cleverness’ should be
introduced in the GUI [how??].

The semantics should be as much as possible agnostic to
communication-means and GUI framework used.

Helper patches
--------------

Helper patches for in/out communication in Pd are needed, these will be
used to dispatch relevant messages to/from Pd.

Use of dollar signs
-------------------

[Dollar signs](http://puredata.info/docs/manuals/pd/x2.htm#s6.5) are
used in Pd to enable the creation of local variables for a
patch/abstraction by using the following notation:

`$0`

`$0` when prepended to something is guaranteed to be unique to the patch
and will be expanded to a number. For example:

`$0-myvar`

could be expanded to something like:

    1003-myvar

All [send] and [receive] used in this context should be prepended with
   
    $0-

This means that one should ensure that the patch communicate the value
of \$0 (which is instantiated on the spot) to the GUI as the first thin.
This will effectively be creating a sort of ‘namespace’ for the patch.
The important thing to remember is that `$0` is always instantiated
(expanded) **dynamically** and will only be known when the patch is
actually loaded by Pd.

GUI -> Pd communication
-----------------------

### Group

A group is a set oaf controls which are conceptually/functionally related, and thus also grouped in the GUI. For example 4 sliders for an ADSR control.

Each control in a group is considered to be unique. That is: more than one receive with the same name is allowed in the patch, but it won’t be duplicated in the GUI.

### Syntax

Each that the Pd patch wishes to expose to the gui follows a syntax of the kind:

`$0-groupname.type.name`

where:

* `$0-` is the dollar sign for keeping the receive local (see [Use of dollar signs]] above)
* groupname is a name for the group, e.g. adsr
* type is the control type, e.g. vslider
* name is the name of the control and will also textual label in the GUI, e.g. attack

groupname type and name will not contain spaces.

- names may use underscores ("_") which the GUI will render as spaces by convention e.g. the name dry signal will be rendered as "dry signal" whrever the GUI is using text 

Example receive could thus be:

    [r $0-adsr.hslider.attack]
    [r $0-adsr.hslider.decay]
    [r $0-reverb.knob.dry_signal]

### Other conventions

### Types of controller 

* hslider: horizontal slider
* vslider: vertical slider
* knob: a knob control
* bang: bang button
* toggle: toggle
* text: text input
* number: numerical input
* range : a control representing a number range
* ...

Pd -> GUI communication
-----------------------

Pd will communicate back to the GUI in similar fashion but using a global send object and specifically constructed messages.
By convention the global send will be labelled

`[send dispatch]`

Of course the dispatcher will know this mechanism and act appropriately.

Whenever Pd has to communicate back to the gui it will send a message to the global sender.

### Groups

As for GUI -> Pd communication, groups are used for logical/functional and thus visual grouping of GUI elements receiving data from Pd.

### Syntax

Messages to be sent to the global sender will be prepended by the string `'togui-'` . This is needed to ease parsing the patch

`togui-groupname.type.name`

where:

* togui~~ is needed to indicate that the message is intended to be communicated to the gui.
* groupname is a name for (and shared) by group, e.g. time
* type is the control type, e.g. number
* name is the name of the control and will also textual label in the GUI, e.g. seconds
Examples:

    [$0-time.number.seconds(
    [$0-levles.VU.main_volume(
    [s $0-array.file(

### Types of elements

The types of elements the GUI should be able to understand from Pd:

* number: a number
* text: text output
text and number could both be handled as text (e.g. ’label’) for output purposes.
* VU: a vumeter control
* array: representation of an array
* ...

Communication techniques
------------------------

The GUI will always send the destination of the message before the message, by convention the destination will be indicated in the message by `"destination"`, and this will accordingly be routed by the dispatcher.

### The dispatcher

The dispatcher is a helper patch which dispatches messages to/from Pd.
It is made of a sender and a receiver part. For example a receiver using
`[netreceive]` and listening on port 12345 of localhost, could look like
this:

    [netreceive 12345]
    |
    [route _destination]
         \   /
          \ / 
           X  
          / \
         /   \
         [send]

Issues and weaknesses
---------------------

- Building the dispatcher by hand would be tedious and error prone... we
need something to do it automatically based on the patch...
What is basically needed is the creation of a routing system for
messages coming in **from** the GUI for example have a ‘dynamic’ send
which will be accordingly fed. Indeed [send] has a right inlet, or this
can be achieved with ; messages...

- The group concept is definitely weak and problematic and will always
have to be tweaked on a per-patch baisis...
